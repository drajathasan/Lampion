<?php
/**
* Custom Menu Layout
*
* Copyright (C) 2018 Drajat Hasan (drajathasan@gmail.com)
* Some code is taken and modified from SLiMS Default template menu generator
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*
*/

function getIcon($iconName) {
	$icon           = array(
	    'home'           => 'fa fa-home',
	    'bibliography'   => 'fa fa-bookmark',
	    'circulation'    => 'fa fa-clock-o',
	    'membership'     => 'fa fa-user',
	    'master_file'    => 'fa fa-pencil',
	    'stock_take'     => 'fa fa-suitcase',
	    'system'         => 'fa fa-keyboard-o',
	    'reporting'      => 'fa fa-file-text-o',
	    'serial_control' => 'fa fa-barcode',
	    'logout'         => 'fa fa-close',
	    'opac'           => 'fa fa-desktop'
	);

	if (isset($icon[$iconName])) {
		return $icon[$iconName];
	}
	return 'fa fa-align-justify';
}


function showModules($dbs)
{
	// Run Query
	$modQuery = $dbs->query('SELECT module_name, module_path, module_desc FROM mst_module');
	// Check
	if ($modQuery->num_rows > 0) {
		$module = '';
		while ($modData = $modQuery->fetch_assoc()) {
			$module_name = ucfirst(str_replace('_', '&nbsp;', $modData['module_name']));
			if (isset($_SESSION['priv'][$modData['module_path']]['r']) && $_SESSION['priv'][$modData['module_path']]['r'] && file_exists('modules'.DS.$modData['module_path'])) {
		 		$module .= '<span data="'.$modData['module_path'].'"><i class="'.getIcon($modData['module_path']).'"></i> '.__($module_name).'</span>';
			}
		}
		return $module;
	}
}

function showSubMenu($module_path)
{
	global $dbs;
	$submenu = MDLBS.$module_path.'/submenu.php';
	$element = '<div id="sidepun" class="right-bar-group-content">';
	$elm 	 = '';
	if (file_exists($submenu)) {
		require_once $submenu;
		foreach ($menu as $key => $value) {
			if ($value[0] == 'Header') {
				$elm .= '<span class="right-bar-header">'.$menu[$key][1].'</span>';
			} else {
				$elm .= $element;
				$elm .= '<label href="'.$menu[$key][1].'"><i class="fa fa-file-text-o"></i> '.$menu[$key][0].'</label>';
				$element = '';
			}
		}
	} else {
		include 'default/submenu.php';
		foreach ($menu as $key => $value) {
			if ($value[0] == 'Header') {
				$elm .= '<span class="right-bar-header">'.$menu[$key][1].'</span>';
			} else {
				$elm .= $element;
				$elm .= '<label><a href="'.$menu[$key][1].'"><i class="fa fa-file-text-o"></i> '.$menu[$key][0].'</a></label>';
				$element = '';
			}
		}
	}
	$elm .= "<script>$('#sidepun label').on('click', function(){
				var href = $(this).attr('href');
				$('#mainContent').simbioAJAX(href);
			});</script>";
	$elm .= "</div>";
	return $elm;
}

function getUserInfo($dbs, $uid)
{
	// Run query
	$uid 	      = $dbs->escape_string($uid);
	$userInfo  	  = $dbs->query('SELECT user_type, last_login, last_login_ip FROM user WHERE user_id = "'.$uid.'"');
	$fetchData 	  = $userInfo->fetch_assoc();
	$userInfoData = array($fetchData['user_type'], $fetchData['last_login'].' via '.$fetchData['last_login_ip']);
	return $userInfoData;
}

function getUserActivity($activityType)
{
	// Take from staff_act.php
	global $dbs;
	$start_date = '2000-01-01';
	$until_date = date('Y-m-d');
	$_count_q = $dbs->query('SELECT COUNT(log_id) FROM system_log WHERE '.$activityType.' AND TO_DAYS(log_date) BETWEEN TO_DAYS(\''.$start_date.'\') AND TO_DAYS(\''.$until_date.'\')');
    $_count_d = $_count_q->fetch_row();
    return $_count_d[0];
}