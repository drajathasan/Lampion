<?php
include 'lampion_function.php';
// Gen Menu
if (isset($_GET['genMenu']) AND !empty($_GET['genMenu'])) {
	$module_path = (string)trim($_GET['genMenu']);
	echo showSubMenu($module_path);
	exit();
}

// User identity
$uid = (isset($_SESSION['uid']))?$_SESSION['uid']:0;
$realname = $dbs->escape_string($_SESSION['realname']);
// Array
// Take from staff_act.php
$userTypeAcitivity = array('log_location=\'bibliography\' AND log_type=\'staff\' AND log_msg LIKE \'%insert bibliographic data%\' AND id=\''.$uid.'\'',
						   'log_location=\'bibliography\' AND log_type=\'staff\' AND log_msg LIKE \'%insert item data%\' AND id=\''.$uid.'\'',
						   'log_location=\'membership\' AND log_type=\'staff\' AND log_msg LIKE \'%add new member%\' AND id=\''.$uid.'\'',
						   'log_location=\'circulation\' AND log_type=\'member\' AND (log_msg LIKE \''.$realname.'%new loan%\')',
						   'log_location=\'circulation\' AND log_type=\'member\' AND (log_msg LIKE \''.$realname.'%return item%\' OR \''.$realname.'%Quick Return%\')',
						   'log_location=\'circulation\' AND log_type=\'member\' AND (log_msg LIKE \''.$realname.'%extend loan%\') AND TO_DAYS(log_date)'
	                      );
// Main variabel
$page_title = 'Lampion';
$user_info  = getUserInfo($dbs, $uid); 
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title><?php echo $page_title; ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate, post-check=0, pre-check=0" />
	<meta http-equiv="Expires" content="Sat, 26 Jul 1997 05:00:00 GMT" />
	<link rel="icon" href="<?php echo SWB; ?>webicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="<?php echo SWB; ?>webicon.ico" type="image/x-icon" />
	<link href="<?php echo SWB; ?>template/core.style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo JWB; ?>colorbox/colorbox.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo JWB; ?>chosen/chosen.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo JWB; ?>jquery.imgareaselect/css/imgareaselect-default.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="<?php echo AWB.'admin_template/lampion/'?>asset/core.css"> 
	<script type="text/javascript" src="<?php echo JWB; ?>jquery.js"></script>
	<script type="text/javascript" src="<?php echo JWB; ?>updater.js"></script>
	<script type="text/javascript" src="<?php echo JWB; ?>gui.js"></script>
	<script type="text/javascript" src="<?php echo JWB; ?>form.js"></script>
	<script type="text/javascript" src="<?php echo JWB; ?>calendar.js"></script>
	<script type="text/javascript" src="<?php echo JWB; ?>ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="<?php echo JWB; ?>keyboard.js"></script>
	<script type="text/javascript" src="<?php echo JWB; ?>chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo JWB; ?>chosen/ajax-chosen.min.js"></script>
	<script type="text/javascript" src="<?php echo JWB; ?>tooltipsy.js"></script>
	<script type="text/javascript" src="<?php echo JWB; ?>colorbox/jquery.colorbox-min.js"></script>
	<script type="text/javascript" src="<?php echo JWB; ?>jquery.imgareaselect/scripts/jquery.imgareaselect.pack.js"></script>
	<script type="text/javascript" src="<?php echo JWB; ?>webcam.js"></script>
	<script type="text/javascript" src="<?php echo JWB; ?>scanner.js"></script>
</head>
<body>
<nav class="main-nav">
	<div class="nav-header-left-content">
		<img class="nav-header-content-child" src="<?php echo AWB;?>admin_template/lampion/asset/logo.png" style="width: 45px;padding: 5px;">
		<div class="modules">
			<?php echo showModules($dbs);?>
		</div>
	</div>
	<div class="nav-header-right-content">
		<a class="nav-header-content-child-link dropdown toLeft" href="#" status="0"><i class="fa fa-user"></i> <?php echo ucwords($_SESSION['uname']);?> <i class="fa fa-sort-desc" style="margin-left: 10px;"></i></a>
		<a class="nav-header-content-child-link toLeft" href="#" style="margin-top: 4px;"><i class="fa fa-bell"></i></a>
		<a class="nav-header-content-child-link toLeft" href="logout.php"><i class="fa fa-sign-out"></i> Logout</a>
	</div>
	<div class="drop-down-menu" style="display: none;">
		<span>Hai</span>
		<span>Hai</span>
		<span>Hai</span>
	</div>
</nav>
<aside class="sidebar">
   <a class="sideNotclick" title="dashboard" href="index.php"><i class="fa fa-dashboard"></i></a>
   <a class="opac" title="opac" href="#" link="../index.php"><i class="fa fa-desktop"></i></a>
   <a class="sideBarLink" title="User Setting" href="#" link="<?php echo  MWB.'system/app_user.php?changecurrent=true&action=detail';?>"><i class="fa fa-user"></i></a>
</aside>
<div class="right-bar animated fadeIn col-xs-12 col-sm-12 col-md-3 col-lg-3">
	<div class="right-bar-group dashbord-bar">
		<span class="right-bar-header">General Information</span>
		<div class="right-bar-group-content">
			<label>Current User</label>
			<span><?php echo ucwords($_SESSION['uname']);?></span>
		</div>
		<div class="right-bar-group-content">
			<label>User Type</label>
			<span><?php echo $sysconf['system_user_type'][$user_info[0]];?></span>
		</div>
		<div class="right-bar-group-content">
			<label>Last Login</label>
			<span><?php echo $user_info[1];?></span>
		</div>
	</div>
	<br>
	<div class="right-bar-group dashbord-bar">
		<span class="right-bar-header">Self Activity</span>
		<div class="right-bar-group-content">
			<label>Bibliography Data Entry</label>
			<span><?php echo getUserActivity($userTypeAcitivity[0]);?></span>
			<label>Item Data Entry</label>
			<span><?php echo getUserActivity($userTypeAcitivity[1]);?></span>
			<label>Member Data Entry</label>
			<span><?php echo getUserActivity($userTypeAcitivity[2]);?></span>
			<label>Loans</label>
			<span><?php echo getUserActivity($userTypeAcitivity[3]);?></span>
			<label>Return</label>
			<span><?php echo getUserActivity($userTypeAcitivity[4]);?></span>
			<label>Extends</label>
			<span><?php echo getUserActivity($userTypeAcitivity[5]);?></span>
		</div>
	</div>
</div>
<div class="loader center-bar-header">
	<?php echo $info;?>
</div>
<div id="mainContent" class="animated fadeIn">
	<?php 
	if (!isset($_GET['mod'])) {
		include 'lampion_dashboard.php';
	}
	?>
</div>
<!-- fake submit iframe for search form, DONT REMOVE THIS! -->
<iframe name="blindSubmit" style="visibility: hidden; width: 0; height: 0;"></iframe>
<!-- fake submit iframe -->
<script type="text/javascript">
    <?php 
    if (isset($_GET['mod']) AND $_GET['mod'] == 'system') {
    	echo "// Redirect if get mod"."\n";
    	echo "$('#mainContent').simbioAJAX('modules/system/index.php');"."\n";
    	echo "\n";
    }
    ?>
	$('.modules span').on('click', function(){
		var data = $(this).attr('data');
		// Set MainContent
		$('#mainContent').simbioAJAX('modules/'+data+'/index.php');
		// Submenu
		$('.right-bar').simbioAJAX('index.php?genMenu='+data);
	});

	$('.sideBarLink').on('click', function(){
		var link = $(this).attr('link');
		$('#mainContent').simbioAJAX(link);
	});

	$('.opac').on('click', function(evt){
		evt.preventDefault();
    	top.jQuery.colorbox({iframe:true,
    	  href: $(this).attr('link'),
          width: function() { return parseInt($(window).width())-50; },
          height: function() { return parseInt($(window).height())-50; },
          title: function() { return 'Online Public Access Catalog'; } }
        );
    });

    $('.dropdown').on('click', function(){
    	var status = $(this).attr('status');
    	if (status == 0) {
    		$(this).attr('status', 1);
    		$('.drop-down-menu').slideDown();
    	} else {
    		$(this).attr('status', 0);
    		$('.drop-down-menu').slideUp();
    	}
    });
</script>
</body>
</html>